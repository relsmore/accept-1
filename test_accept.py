#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Accept Test Suite."""

import os
import unittest
import accept


class AcceptParserTestCase(unittest.TestCase):
    """Accept Parser test cases."""

    def test_empty_value(self):
        self.assertEquals(accept.parse(''), [])

    def test_none_value(self):
        self.assertEquals(accept.parse(None), [])

    def test_parse_simple_header(self):
        m = [accept.MediaType('application/json')]
        self.assertEquals(m, accept.parse('application/json'))

    def test_accept_header_still_included(self):
        m = [accept.MediaType('application/json')]
        self.assertEquals(m, accept.parse('Accept: application/json'))

    def test_prefer_most_specific_type(self):
        m = [
            accept.MediaType('application/json'),
            accept.MediaType('application/*', 0.2),
        ]
        self.assertEquals(
            accept.parse('application/*; q=0.2, application/json'),
            m
        )

    def test_media_type_parameter_with_quotes(self):
        self.assertEquals(
            accept.parse('application/*; q="0.2"'),
            [accept.MediaType('application/*', 0.2)]
        )
        self.assertEquals(
            accept.parse("application/*; q='0.2'"),
            [accept.MediaType('application/*', 0.2)]
        )
        self.assertEquals(
            accept.parse('application/*; q=0.2; test="moop"'),
            [accept.MediaType('application/*', 0.2, {"test": "moop"})]
        )
        self.assertEquals(
            accept.parse("application/*; q=0.2; test='moop'"),
            [accept.MediaType('application/*', 0.2, {"test": "moop"})]
        )

    def test_special_characters(self):
        self.assertEquals(
            accept.parse('application/*; test=_0-2'),
            [accept.MediaType('application/*', params={"test": "_0-2"})]
        )
        self.assertEquals(
            accept.parse("application/*; test=_0-2'"),
            [accept.MediaType('application/*', params={"test": "_0-2"})]
        )

    def test_non_valid_q_value(self):
        self.assertEquals(
            accept.parse('application/*; q=_0-2'),
            [accept.MediaType('application/*', 1.0)]
        )

    def test_elaborate_accept_header(self):
        self.assertEquals(
            accept.parse('text/*, text/html, text/html;level=1, */*'),
            [
                accept.MediaType('text/html', params={'level': '1'}),
                accept.MediaType('text/html'),
                accept.MediaType('text/*'),
                accept.MediaType('*/*')
            ]
        )

    def test_real_world_header(self):
        m = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        self.assertEquals(
            accept.parse(m),
            [
                accept.MediaType('text/html'),
                accept.MediaType('application/xhtml+xml'),
                accept.MediaType('application/xml', q=0.9),
                accept.MediaType('*/*', q=0.8)
            ]
        )

    def test_parse_broken_accept_header(self):
        header = ('text/xml,application/xml,application/xhtml+xml,') +\
                 ('text/html;q=0.9,text/plain;q=0.8,image/*,,*/*;q=0.5')
        self.assertEquals(
            accept.parse(header),
            [
                accept.MediaType('text/xml'),
                accept.MediaType('application/xml'),
                accept.MediaType('application/xhtml+xml'),
                accept.MediaType('image/*'),
                accept.MediaType('text/html', q=0.9),
                accept.MediaType('text/plain', q=0.8),
                accept.MediaType('*/*', q=0.5)
            ]
        )


class AcceptMediaTypeTestCase(unittest.TestCase):
    """Accept Media Type test cases."""

    def test_equal_media_types(self):
        self.assertEquals(
            accept.MediaType('application/json'),
            accept.MediaType('application/json'),
        )
        self.assertEquals(
            accept.MediaType('application/json', params={'test': '2'}),
            accept.MediaType('application/json', params={'test': '2'}),
        )

    def test_more_specific(self):
        self.assertLess(
            accept.MediaType('application/*'),
            accept.MediaType('text/plain', q=0.8),
        )
        self.assertLess(
            accept.MediaType('application/json', params={'test': '2'}),
            accept.MediaType('application/*'),
        )
        self.assertLess(
            accept.MediaType('application/json'),
            accept.MediaType('application/*'),
        )
        self.assertLess(
            accept.MediaType('application/*'),
            accept.MediaType('*/*'),
        )

    def test_less_specific(self):
        self.assertGreater(
            accept.MediaType('text/plain', q=0.8),
            accept.MediaType('application/*')
        )
        self.assertGreater(
            accept.MediaType('application/*'),
            accept.MediaType('application/json', params={'test': '2'}),
        )
        self.assertGreater(
            accept.MediaType('application/*'),
            accept.MediaType('application/json'),
        )
        self.assertGreater(
            accept.MediaType('*/*'),
            accept.MediaType('application/*')
        )

    def test_all_subtypes(self):
        self.assertFalse(accept.MediaType('application/json').all_subtypes)
        self.assertTrue(accept.MediaType('application/*').all_subtypes)
        self.assertTrue(accept.MediaType('*/*').all_subtypes)

    def test_all_types(self):
        self.assertFalse(accept.MediaType('application/json').all_types)
        self.assertFalse(accept.MediaType('application/*').all_types)
        self.assertTrue(accept.MediaType('*/*').all_types)

    def test_string_representation(self):
        m = accept.parse(
            'application/json; q=0.2; level=1; test=2; something=3;'
        )[0]
        self.assertEquals(
            str(m),
            'application/json; q=0.2; level=1; test=2; something=3'
        )


if __name__ == '__main__':
    unittest.main()
